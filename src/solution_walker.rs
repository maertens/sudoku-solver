use sudoku::*;

pub struct SolutionWalker {
    sudoku: Sudoku,
    position: (usize, usize),
    numbers: Vec<Num>,
}

impl SolutionWalker {
    pub fn new(sudoku: Sudoku, position: (usize, usize)) -> SolutionWalker {
        let numbers = sudoku.options_left(position);
        SolutionWalker {
            sudoku,
            position,
            numbers
        }
    }

    pub fn next(&mut self) -> Option<Sudoku> {
        self.numbers
            .pop()
            .map(|num| self.sudoku.with_filled_in(self.position, num))
    }
}

