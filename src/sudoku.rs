use std::ops::{Index, IndexMut};

use bitset::*;
use solution_walker::*;
use sudoku_solver::*;

pub type Num = u8;
pub type Pos = (usize, usize);

pub const SIZE : usize = 9;

#[derive(Debug,PartialEq,Eq,Clone)]
pub struct Sudoku([[Num; SIZE]; SIZE]);

impl Index<usize> for Sudoku {
    type Output = [Num; SIZE];

    fn index(&self, i : usize) -> &[Num; SIZE] {
        &self.0[i]
    }
}

impl IndexMut<usize> for Sudoku {
    fn index_mut(&mut self, i : usize) -> &mut [Num; SIZE] {
        &mut self.0[i]
    }
}

impl Sudoku {

    pub fn new(grid : [[Num; SIZE]; SIZE]) -> Sudoku {
        Sudoku(grid)
    }

    pub fn empty_iter<'a>(&'a self) -> impl Iterator<Item = Pos> + 'a {
        (0..SIZE).flat_map(move |i| self.row_iter(i)
                        .enumerate()
                        .filter(|(_, &v)| v == 0)
                        .map(move |(j, _)| (i, j)))
              .into_iter()
    }

    pub fn row_iter(&self, i : usize) -> impl Iterator<Item = &Num> {
        debug_assert!(i < SIZE);
        self[i].iter()
    }

    pub fn col_iter<'a>(&'a self, i : usize) -> impl Iterator<Item = &Num> + 'a  {
        debug_assert!(i < SIZE);
        self.0.iter().map(move |row| &row[i]).into_iter()
    }

    pub fn sqr_iter(&self, i : usize) -> impl Iterator<Item = &Num> {
        debug_assert!(i < SIZE);
        let (sqr_row, sqr_col) = (i / 3, i % 3);
        let sqr_row_start = sqr_row*3;
        let sqr_col_start = sqr_col*3;
        self.0[sqr_row_start..sqr_row_start+3]
            .iter()
            .flat_map(move |row| row[sqr_col_start..sqr_col_start+3].iter())
            .into_iter()
    }

    fn valid_iter<'a, I>(mut numbers :  I) -> bool 
        where I: Iterator<Item = &'a Num> {
        let mut bs = BitSet::new();
        numbers.all(|&x| x == 0 || bs.put(x - 1))
    }

    pub fn valid_row(&self, i : usize) -> bool {
        Sudoku::valid_iter(self.row_iter(i))
    }

    pub fn valid_col(&self, i : usize) -> bool {
        Sudoku::valid_iter(self.col_iter(i))
    }

    pub fn valid_sqr(&self, i : usize) -> bool {
        Sudoku::valid_iter(self.sqr_iter(i))
    }

    pub fn valid(&self) -> bool {
        (0..SIZE).all(|i| self.valid_row(i) &
                       self.valid_col(i) &&
                       self.valid_sqr(i))
    }

    pub fn options_left(&self, (row, col) : Pos) -> Vec<Num> {
        let sqr = (row / 3) * 3 + (col / 3);
        let row_used = BitSet::all_in(self.row_iter(row));
        let col_used = BitSet::all_in(self.col_iter(col));
        let sqr_used = BitSet::all_in(self.sqr_iter(sqr));
        let all = row_used | col_used | sqr_used;
        (1..=9).filter(move |&i| !all.has(i)).collect()
    }

    pub fn with_filled_in(&self, (row, col) : Pos, val : Num) -> Sudoku {
        let mut new_sudoku = self.clone();
        new_sudoku[row][col] = val;
        return new_sudoku
    }


    pub fn solutions_for_location(self, position: Pos) -> SolutionWalker {
        SolutionWalker::new(self, position)
    }

    pub fn solution(self) -> Option<Sudoku> {
        SudokuSolver::new(self).next()
    }

    pub fn solutions(self) -> Vec<Sudoku> {
        SudokuSolver::new(self).collect()
    }
}


#[cfg(test)]
mod test {
    use sudoku::*;

    const TEST_SUDOKU : Sudoku = Sudoku([
        [0,0,3, 0,2,0, 6,0,0],
        [9,0,0, 3,0,5, 0,0,1],
        [0,0,1, 8,0,6, 4,0,0],

        [0,0,8, 1,0,2, 9,0,0],
        [7,0,0, 0,0,0, 0,0,8],
        [0,0,6, 7,0,8, 2,0,0],

        [0,0,2, 6,0,9, 5,0,0],
        [8,0,0, 2,0,3, 0,0,9],
        [0,0,5, 0,1,0, 3,0,0]
    ]);

    const TEST_SUDOKU_SOLUTION : Sudoku = Sudoku([
        [4,8,3, 9,2,1, 6,5,7],
        [9,6,7, 3,4,5, 8,2,1],
        [2,5,1, 8,7,6, 4,9,3],

        [5,4,8, 1,3,2, 9,7,6],
        [7,2,9, 5,6,4, 1,3,8],
        [1,3,6, 7,9,8, 2,4,5],

        [3,7,2, 6,8,9, 5,1,4],
        [8,1,4, 2,5,3, 7,6,9],
        [6,9,5, 4,1,7, 3,8,2]
    ]);

    #[test]
    fn iterators(){
        let sudoku = TEST_SUDOKU.clone();
        let row : Vec<Num> = sudoku.row_iter(1).map(|&x| x).collect();
        assert_eq!(row, vec![9, 0, 0, 3, 0, 5, 0, 0, 1]);

        let col : Vec<Num> = sudoku.col_iter(2).map(|&x| x).collect();
        assert_eq!(col, vec![3, 0, 1, 8, 0, 6, 2, 0, 5]);

        let sqr : Vec<Num> = sudoku.sqr_iter(3).map(|&x| x).collect();
        assert_eq!(sqr, vec![0, 0, 8, 7, 0, 0, 0, 0, 6]);
    }


    #[test]
    fn valid_row(){
        let mut sudoku = TEST_SUDOKU.clone();
        assert_eq!(sudoku.valid_row(0), true);

        sudoku[5][0] = 4;
        assert_eq!(sudoku.valid_row(5), true);

        sudoku[3][0] = 2;
        assert_eq!(sudoku.valid_row(3), false);
    }

    #[test]
    fn valid_col(){
        let mut sudoku = TEST_SUDOKU.clone();
        assert_eq!(sudoku.valid_col(0), true);

        sudoku[0][3] = 4;
        assert_eq!(sudoku.valid_col(3), true);

        sudoku[0][5] = 5;
        assert_eq!(sudoku.valid_col(5), false);
    }

    #[test]
    fn valid_sqr(){
        let mut sudoku = TEST_SUDOKU.clone();
        assert_eq!(sudoku.valid_sqr(0), true);

        sudoku[1][1] = 6;
        assert_eq!(sudoku.valid_sqr(0), true);

        sudoku[1][7] = 4;
        assert_eq!(sudoku.valid_sqr(2), false);
    }

    #[test]
    fn valid(){
        assert_eq!(TEST_SUDOKU.valid(), true);
        assert_eq!(TEST_SUDOKU_SOLUTION.valid(), true);

        let mut sudoku = TEST_SUDOKU.clone();
        sudoku[8][8] = 9;
        assert_eq!(sudoku.valid(), false);
    }

    #[test]
    fn empty_iter(){
        let mut sudoku = TEST_SUDOKU_SOLUTION.clone();
        let empty_places = vec![(0, 0), (0, 8), (3, 3), (5, 7), (6, 2), (8, 8)];
        for &(i, j) in &empty_places {
            sudoku[i][j] = 0;
        }
        let empty_found : Vec<Pos> = sudoku.empty_iter().collect();
        assert_eq!(empty_found.len(), empty_places.len());
        for (i, place) in sudoku.empty_iter().enumerate() {
            assert_eq!(place, empty_places[i]);
        }
    }

    #[test]
    fn solution(){
        let mut sudoku = TEST_SUDOKU.clone();
        assert_eq!(sudoku.solution(), Some(TEST_SUDOKU_SOLUTION));
    }

    #[test]
    fn solutions(){
        let sudoku = Sudoku([
            [9,0,6, 0,7,0, 4,0,3],
            [0,0,0, 4,0,0, 2,0,0],
            [0,7,0, 0,2,3, 0,1,0],

            [5,0,0, 0,0,0, 1,0,0],
            [0,4,0, 2,0,8, 0,6,0],
            [0,0,3, 0,0,0, 0,0,5],

            [0,3,0, 7,0,0, 0,5,0],
            [0,0,7, 0,0,5, 0,0,0],
            [4,0,5, 0,1,0, 7,0,8]
        ]);

        let sudoku_solutions = vec![
            Sudoku([
                [9,2,6, 5,7,1, 4,8,3],
                [3,5,1, 4,8,6, 2,7,9],
                [8,7,4, 9,2,3, 5,1,6], 

                [5,8,2, 3,6,7, 1,9,4],
                [1,4,9, 2,5,8, 3,6,7],
                [7,6,3, 1,9,4, 8,2,5],

                [2,3,8, 7,4,9, 6,5,1],
                [6,1,7, 8,3,5, 9,4,2],
                [4,9,5, 6,1,2, 7,3,8]
            ]),
            Sudoku([
                [9,2,6, 5,7,1, 4,8,3],
                [3,5,1, 4,8,6, 2,7,9],
                [8,7,4, 9,2,3, 5,1,6], 

                [5,8,2, 3,6,7, 1,9,4],
                [1,4,9, 2,5,8, 3,6,7],
                [7,6,3, 1,4,9, 8,2,5],

                [2,3,8, 7,9,4, 6,5,1],
                [6,1,7, 8,3,5, 9,4,2],
                [4,9,5, 6,1,2, 7,3,8]
            ]),
        ];

        assert_eq!(sudoku.solutions(), sudoku_solutions);
    }
}
