use std::ops::BitOr;

type Bits = u16;

#[derive(Debug,PartialEq,Eq)]
pub struct BitSet(Bits);

impl BitSet {

    pub fn new() -> BitSet {
        BitSet(0)
    }

    // returns true if elem is in this bitset
    pub fn has(&self, elem : u8) -> bool {
        (self.0 & (1 << elem)) != 0
    }

    // inserts elem in this bitset, retuns true if
    // the item was not already in this bitset
    pub fn put(&mut self, elem : u8) -> bool{
        let old = self.0;
        self.0 |= 1 << elem;
        old != self.0
    }

    pub fn all_in<'a, I>(numbers : I) -> BitSet
        where I: Iterator<Item = &'a u8> {
        let mut bs = BitSet::new();
        for &number in numbers {
            bs.put(number);
        }
        return bs
    }
}

impl BitOr for BitSet {
    type Output = Self;

    fn bitor(self, other: Self) -> Self {
        BitSet(self.0 | other.0)
    }
}

#[cfg(test)]
mod test {
    use bitset::*;

    #[test]
    fn has(){
        let bs = BitSet(2);
        assert_eq!(bs.has(0), false);
        assert_eq!(bs.has(1), true);
        assert_eq!(bs.has(2), false);
    }

    #[test]
    fn put(){
        let mut bs = BitSet(2);
        let res = bs.put(2);
        assert_eq!(res, true);
        assert_eq!(bs, BitSet(6));

        let res = bs.put(2);
        assert_eq!(res, false);
        assert_eq!(bs, BitSet(6));
    }
}
