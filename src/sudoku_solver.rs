
use sudoku::*;
use solution_walker::*;

pub struct SudokuSolver {
    empty_positions : Vec<Pos>,
    backtrack : Vec<SolutionWalker>,
}

impl SudokuSolver {
    pub fn new(sudoku : Sudoku) -> SudokuSolver {
        let empty_positions : Vec<Pos> = sudoku.empty_iter().collect();
        let mut backtrack : Vec<SolutionWalker> = Vec::new();
        if let Some(pos) = empty_positions.first() {
            backtrack.push(sudoku.solutions_for_location(*pos));
        }
        SudokuSolver {
            empty_positions,
            backtrack,
        }
    }
}

impl Iterator for SudokuSolver {
    type Item = Sudoku;
    fn next(&mut self) -> Option<Sudoku> {
        loop {
            match self.backtrack.pop() {
                Some(mut walker) => {
                    match walker.next() {
                        Some(sudoku) => {
                            let filled = self.backtrack.len() + 1;
                            if filled == self.empty_positions.len() {
                                return Some(sudoku)
                            } else {
                                let pos = self.empty_positions[filled];
                                let next_walker = sudoku.solutions_for_location(pos);
                                self.backtrack.push(walker);
                                self.backtrack.push(next_walker);
                            }
                        },
                        None => ()
                    }
                },
                None => return None
            }
        }
    }
}
